// helloworld
//
#include <iostream>
#include "stdlib.h" // for atoi

int main(int argc, char *argv[])
{
  int i = 0;

    std::cout << argc << std::endl;
    std::cout << argv[0] << std::endl;
     /*
     argv[0]  char*
     argv[1]  char*
     */
    
    if (argc == 2) {
        std::cout << "argv[1]: " << argv[1] << std::endl;
        i = atoi(argv[1]);
        std::cout << "i: table " << i << std::endl;
    } else {
        std::cout << "Invalid number of arguments" << std::endl;
        return 1;
    }
   
    
	std::cout << "Hello, world! :-)" << std::endl;
	return 0;
}

